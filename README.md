# µRT Performance Benchmarks

Contents of this directory:

- Benchmark result data of the µRT benchmark application [1,2] for four different microcontrollers:
  - [NUCLEO-F401RE](NUCLEO-F401RE.zip): STM32F4 (ARM Cortex-M4)
  - [NUCLEO-F767ZI](NUCLEO-F767ZI.zip): STM32F7 (ARM Cortex-M7)
  - [NUCLEO-G071RB](NUCLEO-G071RB.zip): STM32G0 (ARM Cortex-M0+)
  - [NUCLEO-L476RG](NUCLEO-L476RG.zip): STM32L4 (ARM Cortex-M4)
- [`LICENSE`](LICENSE) file:<br>
  Data may be accessed, modified and republished according to the Creative Commons Attribution 4.0 International license (CC-BY).

---

**[1]** Schöpping T, Kenneweg S. "µRT". Bielefeld University; 2022. DOI: [10.4119/unibi/2966336](https://doi.org/10.4119/unibi/2966336)

**[2]** https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps/-/tree/main/configurations/uRT-Benchmark
